%global app                     ices
%global d_bin                   %{_bindir}

Name:                           meta-ices
Version:                        1.0.0
Release:                        7%{?dist}
Summary:                        META-package for install and configure IceS
License:                        GPLv3

Source10:                       %{app}.local.xml
Source20:                       app.%{app}.playlist.sh

Requires:                       ices

%description
META-package for install and configure IceS.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{_sysconfdir}/%{app}.local.xml
%{__install} -Dp -m 0755 %{SOURCE20} \
  %{buildroot}%{d_bin}/app.%{app}.playlist.sh


%files
%config %{_sysconfdir}/%{app}.local.xml
%{d_bin}/app.%{app}.playlist.sh


%changelog
* Thu Aug 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-7
- UPD: Shell scripts.

* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-6
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-5
- UPD: Scripts & configs.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-4
- UPD: SPEC-file.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- NEW: 1.0.0-3.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- NEW: 1.0.0-2.

* Fri Feb 15 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
